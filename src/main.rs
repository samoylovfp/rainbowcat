use rand::prelude::*;
use std::{alloc::System, io::{BufReader, BufRead}, fs::File};

// to reduce binary size
#[global_allocator]
static A: System = System;

use hsl::HSL;
use std::{
    io::{stdin, Write},
    process::exit,
};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

fn main() {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);
    let mut buffer = String::new();
    for mut file in get_requested_files() {
        // start with a random color
        let mut line_hue: u32 = thread_rng().gen_range(0..360);
        let line_hue_step = 10;
        let per_char_step = 2;

        loop {
            match file.read_line(&mut buffer) {
                Ok(0) => exit(0),
                Ok(_) => {
                    print_colored(line_hue, &buffer, &mut stdout, per_char_step);
                    line_hue += line_hue_step;
                    buffer.clear();
                }
                Err(e) => {
                    println!("{e}");
                    exit(1)
                }
            }
        }
    }
}

fn get_requested_files() -> impl Iterator<Item=Box<dyn BufRead>> {
    let args = std::env::args().collect::<Vec<String>>();
    let reader: Box<dyn BufRead> =
    if args.len() > 1 {
            Box::new(
                BufReader::new(
                File::open(&args[1]).unwrap()
                )
            )
        
    } else {
    Box::new(BufReader::new(stdin()))
    };

    std::iter::once(reader)
}

fn print_colored(mut hue: u32, buffer: &str, stdout: &mut StandardStream, per_char_step: u32) {
    for char in buffer.chars() {
        hue = (hue + per_char_step) % 360;
        let color = HSL {
            h: hue as f64,
            s: 0.6,
            l: 0.5,
        }
        .to_rgb();
        stdout
            .set_color(ColorSpec::new().set_fg(Some(Color::Rgb(color.0, color.1, color.2))))
            .unwrap();

        write!(stdout, "{char}").unwrap();
    }
}
